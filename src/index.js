import React from 'react';
import ReactDOM from 'react-dom';

import './index.css'

const data = [
    {
        image: 'https://m.media-amazon.com/images/I/51PkT7w5SXL.jpg',
        title:'Dead Ringer:Truth Seekers Book',
        author: 'Susan Sleeman'
    },
    {
        image:'https://m.media-amazon.com/images/I/51PAIR77wJL.jpg',
        title:'The Lean Startup',
        author:'Eric Ries'
    }
]
const firstBook = {
    image: 'https://m.media-amazon.com/images/I/51PkT7w5SXL.jpg',
    title:'Dead Ringer:Truth Seekers Book',
    author: 'Susan Sleeman'
}

const secondBook = {
    image:'https://m.media-amazon.com/images/I/51PAIR77wJL.jpg',
    title:'The Lean Startup',
    author:'Eric Ries'
}

function BookList(){
    return (
        <section className='booklist'>
            <Book img={firstBook.image}
                  title={firstBook.title}
                  author={firstBook.author}
            />
            <Book
                img={secondBook.image}
                title={secondBook.title}
                author={secondBook.author}
            />
        </section>
    )
}


const Book = ({ img, title, author, children }) => {
    return (
        <article className='book'>
            <img src={img} align='no image' />
            <h1>{title}</h1>
            <h4>{author}</h4>
            {children}
        </article>
    )
}



ReactDOM.render(<BookList />,  document.getElementById('root')
);

